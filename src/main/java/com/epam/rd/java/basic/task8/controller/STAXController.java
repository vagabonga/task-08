package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Flower;

import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.stream.*;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private final String xmlFileName;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public List<Flower> parseStAX() {
		List<Flower> flowers = new ArrayList<>();
		XMLInputFactory factory = XMLInputFactory.newInstance();
		factory.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, true);

		XMLEventReader reader = null;

		Flower it = null;
		try {
			reader = factory.createXMLEventReader(new StreamSource(xmlFileName));

			while(reader.hasNext()) {
				XMLEvent ev = reader.nextEvent();
				if(ev.isStartElement()) {
					StartElement start = ev.asStartElement();

					switch (start.getName().getLocalPart()) {
						case "flower": {
							it = new Flower();
							break;
						}
						case "name": {
							ev = reader.nextEvent();
							if(ev.isCharacters()) it.setName(ev.asCharacters().getData());
							break;
						}
						case "soil": {
							ev = reader.nextEvent();
							if(ev.isCharacters()) it.setSoil(ev.asCharacters().getData());
							break;
						}
						case "origin": {
							ev = reader.nextEvent();
							if(ev.isCharacters()) it.setOrigin(ev.asCharacters().getData());
							break;
						}
						case "stemColour": {
							ev = reader.nextEvent();
							if(ev.isCharacters()) it.setStemColour(ev.asCharacters().getData());
							break;
						}
						case "leafColour": {
							ev = reader.nextEvent();
							if(ev.isCharacters()) it.setLeafColour(ev.asCharacters().getData());
							break;
						}
						case "aveLenFlower": {
							ev = reader.nextEvent();
							if(ev.isCharacters()) it.setAveLenFlower(ev.asCharacters().getData());
							break;
						}
						case "tempreture": {
							ev = reader.nextEvent();
							if(ev.isCharacters()) it.setTempreture(Integer.parseInt(ev.asCharacters().getData()));
							break;
						}
						case "lighting": {
							Attribute light = start.getAttributeByName(new QName("lightRequiring"));
							it.setLighting(light.getValue());
							break;
						}
						case "watering": {
							ev = reader.nextEvent();
							if(ev.isCharacters()) it.setWatering(Integer.parseInt(ev.asCharacters().getData()));
							break;
						}
						case "multiplying" : {
							ev = reader.nextEvent();
							if(ev.isCharacters()) it.setMultiplying(ev.asCharacters().getData());
						}
					}
				}
				if(ev.isEndElement()) {
					EndElement end = ev.asEndElement();
					if(end.getName().getLocalPart().equals("flower")) flowers.add(it);
				}
			}

		} catch (XMLStreamException e) {
			e.printStackTrace();
		} finally {
			try {
				reader.close();
			} catch (XMLStreamException e) {
				e.printStackTrace();
			}
		}
		return flowers;
	}

	public void sort(List<Flower> flowers) {
		flowers.sort(new Comparator<>() {
			@Override
			public int compare(Flower o1, Flower o2) {
				int isNamesEquals = o1.getName().compareTo(o2.getName());
				if(isNamesEquals == 0) {
					return o1.getOrigin().compareTo(o2.getOrigin());
				}
				return isNamesEquals;
			}
		});
	}

	public void writeStAX(List<Flower> flowers, String outputXmlFile) {
		try {
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			writeXML(out, flowers);
			String xml = out.toString(StandardCharsets.UTF_8);
			String prettyPrint = formatPrint(xml);
			Files.writeString(Paths.get(outputXmlFile),
					prettyPrint, StandardCharsets.UTF_8);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void writeXML(OutputStream out, List<Flower> flowers) {
		XMLOutputFactory factory = XMLOutputFactory.newInstance();
		XMLStreamWriter writer = null;
		try {
			writer = factory.createXMLStreamWriter(out);

			writer.writeStartDocument("UTF-8", "1.0");
			writer.writeStartElement("flowers");
			writer.writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
			writer.writeAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd");

			writer.writeDefaultNamespace("http://www.nure.ua");

			for (Flower flower : flowers) {

				//<flower>
				writer.writeStartElement("flower");

				//<name>
				writer.writeStartElement("name");
				writer.writeCharacters(flower.getName());
				writer.writeEndElement();
				//</name>

				//<soil>
				writer.writeStartElement("soil");
				writer.writeCharacters(flower.getSoil());
				writer.writeEndElement();
				//</soil>

				//<origin>
				writer.writeStartElement("origin");
				writer.writeCharacters(flower.getOrigin());
				writer.writeEndElement();
				//</origin>

				//<visualParameters>
				writer.writeStartElement("visualParameters");

				//<stemColour>
				writer.writeStartElement("stemColour");
				writer.writeCharacters(flower.getStemColour());
				writer.writeEndElement();
				//</stemColour>

				//<leafColour>
				writer.writeStartElement("leafColour");
				writer.writeCharacters(flower.getLeafColour());
				writer.writeEndElement();
				//</leafColour>

				//<aveLenFlower>
				writer.writeStartElement("aveLenFlower");
				writer.writeAttribute("measure", "cm");
				writer.writeCharacters(flower.getAveLenFlower());
				writer.writeEndElement();
				//</aveLenFlower>

				writer.writeEndElement();
				//</visualParameters>


				//<growingTips>
				writer.writeStartElement("growingTips");

				//<tempreture>
				writer.writeStartElement("tempreture");
				writer.writeAttribute("measure", "celcius");
				writer.writeCharacters(String.valueOf(flower.getTempreture()));
				writer.writeEndElement();
				//</tempreture>

				//<lighting>
				writer.writeEmptyElement("lighting");
				writer.writeAttribute("lightRequiring", flower.getLighting());
				//</lighting>

				//<watering>
				writer.writeStartElement("watering");
				writer.writeAttribute("measure", "mlPerWeek");
				writer.writeCharacters(String.valueOf(flower.getWatering()));
				writer.writeEndElement();
				//</watering>

				writer.writeEndElement();
				//</growingTips>


				//<multiplying>
				writer.writeStartElement("multiplying");
				writer.writeCharacters(flower.getMultiplying());
				writer.writeEndElement();
				//</multiplying>


				writer.writeEndElement();
				//</flower>
			}

			writer.writeEndElement();
			writer.writeEndDocument();
		} catch (XMLStreamException e) {
			e.printStackTrace();
		} finally {
			try {
				assert writer != null;
				writer.flush();
				writer.close();
			} catch (XMLStreamException e) {
				e.printStackTrace();
			}
		}

	}
	private String formatPrint(String xmlFile) {
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer;
		StreamSource source;
		StringWriter output = null;
		try {
			transformer = transformerFactory.newTransformer();

			//print by indention
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");

			//adding end line
			transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");

			source = new StreamSource(new StringReader(xmlFile));
			output = new StringWriter();
			transformer.transform(source, new StreamResult(output));

		} catch (TransformerException e) {
			e.printStackTrace();
		}

		assert output != null;
		return output.toString();
	}
}