package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.Flower;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import java.io.File;
import java.io.IOException;
import java.util.*;


/**
 * Controller for DOM parser.
 */
public class DOMController {

	private final String xmlFileName;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE
	public List<Flower> parseDOM() throws ParserConfigurationException, IOException, SAXException {
		List<Flower> flowers = new ArrayList<>();

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.parse(new File(xmlFileName));

		NodeList nodes = document.getDocumentElement().getElementsByTagName("flower");

		for (int i = 0; i < nodes.getLength(); i++) {
			Element temp = (Element) nodes.item(i);

			Flower fl = new Flower();
			fl.setName(temp.getElementsByTagName("name").item(0).getTextContent());
			fl.setSoil(temp.getElementsByTagName("soil").item(0).getTextContent());
			fl.setOrigin(temp.getElementsByTagName("origin").item(0).getTextContent());

			fl.setStemColour(temp.getElementsByTagName("stemColour").item(0).getTextContent());
			fl.setLeafColour(temp.getElementsByTagName("leafColour").item(0).getTextContent());
			fl.setAveLenFlower(temp.getElementsByTagName("aveLenFlower").item(0).getTextContent());

			fl.setTempreture(Integer.parseInt(temp.getElementsByTagName("tempreture").item(0).getTextContent()));
			fl.setLighting(temp.getElementsByTagName("lighting").item(0).getAttributes().getNamedItem("lightRequiring").getNodeValue());
			fl.setWatering(Integer.parseInt(temp.getElementsByTagName("watering").item(0).getTextContent()));

			fl.setMultiplying(temp.getElementsByTagName("multiplying").item(0).getTextContent());

			flowers.add(fl);
		}

		return flowers;
	}

	public void sort(List<Flower> flowers) {
		flowers.sort(new Comparator<>() {
			@Override
			public int compare(Flower o1, Flower o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});
	}

	public void writeDOM(List<Flower> flowers, String outputXmlFile) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		try {
			builder = factory.newDocumentBuilder();
			Document document = builder.newDocument();
			Element root = document.createElement("flowers");

			root.setAttribute("xmlns", "http://www.nure.ua");
			root.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
			root.setAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd");

			document.getDocumentURI();
			document.appendChild(root);
			for (Flower flower : flowers) {
				root.appendChild(getFlower(document, flower.getName(),
						flower.getSoil(), flower.getOrigin(),
						flower.getStemColour(), flower.getLeafColour(), flower.getAveLenFlower(),
						flower.getTempreture(), flower.getLighting(), flower.getWatering(), flower.getMultiplying()));
			}
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");

			transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");

			DOMSource source = new DOMSource(document);

			StreamResult file = new StreamResult(new File(outputXmlFile));
			transformer.transform(source, file);

		} catch (ParserConfigurationException | TransformerException e) {
			throw new RuntimeException(e);
		}
	}

	private static Node getFlower(Document doc, String name, String soil, String origin, String stemColour,
								  String leafColour, String aveLenFlower, int tempreture, String lighting,
								  int watering, String multiplying) {
		Element flower = doc.createElement("flower");

		flower.appendChild(getFlowerElements(doc, "name", name));
		flower.appendChild(getFlowerElements(doc, "soil", soil));
		flower.appendChild(getFlowerElements(doc, "origin", origin));

		// Creating visual parameters element
		Element visualParameters = doc.createElement("visualParameters");

		//stemColour
		visualParameters.appendChild(getFlowerElements(doc, "stemColour", stemColour));

		//leafColour
		visualParameters.appendChild(getFlowerElements(doc, "leafColour", leafColour));

		//aveLenFlower
		Element len = doc.createElement("aveLenFlower");
		len.setAttribute("measure", "cm");
		len.appendChild(doc.createTextNode(aveLenFlower));
		visualParameters.appendChild(len);

		flower.appendChild(visualParameters);


		// Creating growingTips element
		Element growingTips = doc.createElement("growingTips");

		//tempreture
		Element t = doc.createElement("tempreture");
		t.setAttribute("measure", "celcius");
		t.appendChild(doc.createTextNode(String.valueOf(tempreture)));
		growingTips.appendChild(t);

		//lighting
		Element l = doc.createElement("lighting");
		l.setAttribute("lightRequiring", lighting);
		growingTips.appendChild(l);

		//watering
		Element w = doc.createElement("watering");
		w.setAttribute("measure", "mlPerWeek");
		w.appendChild(doc.createTextNode(String.valueOf(watering)));
		growingTips.appendChild(w);

		flower.appendChild(growingTips);

		//Creating multiplying element
		flower.appendChild(getFlowerElements(doc, "multiplying", multiplying));
		return flower;
	}

	private static Node getFlowerElements(Document doc, String name, String value) {
		Element node = doc.createElement(name);
		node.appendChild(doc.createTextNode(value));
		return node;
	}

}

