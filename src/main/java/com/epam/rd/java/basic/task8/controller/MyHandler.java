package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Flower;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;


public class MyHandler extends DefaultHandler {
    private final StringBuilder currentValue = new StringBuilder();
    List<Flower> result;
    Flower currentFlower;

    public List<Flower> getResult() {
        return result;
    }

    @Override
    public void startDocument() {
        result = new ArrayList<>();
    }


    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        currentValue.setLength(0);
        if(qName.equalsIgnoreCase("flower")) currentFlower = new Flower();

        if(qName.equalsIgnoreCase("lighting")) {
            String lightRequiring = attributes.getValue("lightRequiring");
            currentFlower.setLighting(lightRequiring);
        }

    }

    @Override
    public void endElement(String uri, String localName, String qName) {

        if (qName.equalsIgnoreCase("name")) currentFlower.setName(currentValue.toString());

        if (qName.equalsIgnoreCase("soil")) currentFlower.setSoil(currentValue.toString());

        if (qName.equalsIgnoreCase("origin")) currentFlower.setOrigin(currentValue.toString());

        if (qName.equalsIgnoreCase("stemColour")) currentFlower.setStemColour(currentValue.toString());

        if (qName.equalsIgnoreCase("leafColour")) currentFlower.setLeafColour(currentValue.toString());

        if (qName.equalsIgnoreCase("aveLenFlower")) currentFlower.setAveLenFlower(currentValue.toString());

        if (qName.equalsIgnoreCase("tempreture")) currentFlower.setTempreture(Integer.parseInt(currentValue.toString()));

        if (qName.equalsIgnoreCase("watering")) currentFlower.setWatering(Integer.parseInt(currentValue.toString()));

        if (qName.equalsIgnoreCase("multiplying")) currentFlower.setMultiplying(currentValue.toString());

        if(qName.equalsIgnoreCase("flower")) result.add(currentFlower);

    }

    @Override
    public void characters(char[] ch, int start, int length) {
        currentValue.append(ch, start, length);
    }
}
