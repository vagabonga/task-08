package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;

import java.util.List;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////

		DOMController domController = new DOMController(xmlFileName);

		List<Flower> fl1 = domController.parseDOM();

		// sort (case 1)
		domController.sort(fl1);

		// save
		String outputXmlFile = "output.dom.xml";
		domController.writeDOM(fl1, outputXmlFile);

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		

		SAXController saxController = new SAXController(xmlFileName);

		List<Flower> fl2 = saxController.parseSAX();

		// sort  (case 2)
		saxController.sort(fl2);

		// save
		outputXmlFile = "output.sax.xml";
		saxController.writeSAX(fl2, outputXmlFile);
		
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		List<Flower> fl3 = staxController.parseStAX();
		
		// sort  (case 3)
		staxController.sort(fl3);
		
		// save
		outputXmlFile = "output.stax.xml";
		staxController.writeStAX(fl3, outputXmlFile);
	}

}
